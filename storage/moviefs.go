package storage

import(
  "log"
  "path/filepath"
  "os"
  "regexp"
)

type MediaFile struct {
  Name string
  Year string
  Path string
}

var mediaFiles []MediaFile

// Scan the directory for media files
func Scan(path string) []MediaFile {
  err := filepath.Walk(path, scanMovies)
  if err != nil {
    log.Println("Scan error: %v", err)
  }
  return mediaFiles
}

// Looks for movies, a movie file is a file matched by the following
// pattern: The Accountant (2016).mkv
func scanMovies(path string, info os.FileInfo, err error) error {
  reg := regexp.MustCompile("^([a-zA-Z0-9\\s-_]+)\\s\\(([0-9]+)\\).([a-zA-Z0-9]{3,4})$")

  if info.IsDir() || info.Name() == "." || info.Name() == ".." {
    return err
  }

  if reg.MatchString(info.Name()) {
    parts := reg.FindStringSubmatch(info.Name())
    file := MediaFile{parts[1], parts[2], path}
    mediaFiles = append(mediaFiles, file)
  }

  return err
}
