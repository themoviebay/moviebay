package storage

import(
  "io/ioutil"
  "encoding/json"
  "strconv"
  "path/filepath"
  "os"
  "regexp"
  "log"
  "fmt"
  "gitlab.com/themoviebay/moviebay/utils"
)

// type definitions
type Movie struct {
  Id int `json:"id"`
  Title string `json:"title"`
  OriginalTitle string `json:"original_title"`
  Overview string `json:"overview"`
  ReleaseDate string `json:"release_date"`
  PosterPath string `json:"poster_path"`
  BackdropPath string `json:"backdrop_path"`
  Converted bool `json:"converted"`
  Filename string `json:"filename"`
  OriginalPath string `json:"original_path"`
  ConvertedPath string `json:"converted_path"`
}

type Moviedb struct {
  data map[int]*Movie
  dbPath string
  convPath string
}

// Singleton pointer to the database
var database *Moviedb

// Returns a human redable name of the structure
func (m *Movie) String() string {
  return fmt.Sprintf("[%d]%s", m.Id, m.Title)
}

// Returns the id as string
func (m *Movie) IdStr() string {
  return strconv.Itoa(m.Id)
}

// Restore database from harddisk
func (db *Moviedb) Restore() {
  walk := func() func(path string, info os.FileInfo, err error) error  {
    return func(path string, info os.FileInfo, err error) error {
      reg := regexp.MustCompile(".json$")
      if reg.MatchString(info.Name()) {
        data, err := ioutil.ReadFile(path)
        check(err)
        movie := &Movie{}
        json.Unmarshal(data, movie)
        if movie.Converted == false {
          convertedFile := db.convPath + "/" + movie.IdStr() + ".mp4"
          if _, err := os.Stat(convertedFile); err == nil {
            movie.Converted = true
            movie.ConvertedPath = convertedFile
            db.SaveMovie(movie)
          }
          check(err)
        }
        db.AddMovie(movie)
      }
      return err
    }
  }()
  err := filepath.Walk(db.dbPath, walk)
  check(err)
}

// Saves the movie to harddisk
func (db *Moviedb) SaveMovie(movie *Movie) error {
  file := db.dbPath + "/" + movie.IdStr() + ".json"
  data, _ := json.Marshal(movie)
  log.Printf("Movie %s saved to: %s\n", movie, file)
  return ioutil.WriteFile(file, data, 0644)
}

// Adds a movie
func (db *Moviedb) AddMovie(movie *Movie) {
  db.data[movie.Id] = movie
}

// Returns the total size of movies in the database
func (db *Moviedb) Size() int {
  return len(db.data)
}

// Returns all movies
func (db *Moviedb) All() []*Movie {
  movies := make([]*Movie, db.Size())
  i := 0
  for _, movie := range db.data {
    movies[i] = movie
    i++
  }
  return movies
}

// FindById looks for a movie with the given id
func (db *Moviedb) FindById(key int) *Movie {
  return db.data[key]
}

// FindByName looks for a movie with the given filepath
func (db *Moviedb) FindByName(filename string) *Movie {
  for _, movie := range db.data {
    if movie.Filename == filename {
      return movie
    }
  }
  return nil
}

// Creates an empty moviedb
func NewMoviedb(dbPath string, convPath string) *Moviedb {
  database = &Moviedb{
    make(map[int]*Movie),
    dbPath,
    convPath,
  }
  return database
}

// Returns an existing moviedb
func LoadMoviedb() *Moviedb {
  return database
}

func NewMovieFromTmdb(tMovie *utils.TmdbMovie, file MediaFile) *Movie {
  return &Movie{
    tMovie.Id,
    tMovie.Title,
    tMovie.Original_title,
    tMovie.Overview,
    tMovie.Release_date,
    tMovie.Poster_path,
    tMovie.Backdrop_path,
    false,
    file.Name,
    file.Path,
    "",
  }
}

func check(err error) {
  if err != nil {
    panic(err)
  }
}
