package main

import(
  "log"
  "os"
  "gitlab.com/themoviebay/moviebay/storage"
  "gitlab.com/themoviebay/moviebay/utils"
  "gitlab.com/themoviebay/moviebay/net"
  "gitlab.com/themoviebay/moviebay/config"
  "gitlab.com/themoviebay/moviebay/converter"
)

var moviedb *storage.Moviedb

func main() {
  args := os.Args[1:]
  config := config.NewConfig(args[0])

  // setup moviedb
  log.Printf("[OK] Starting Moviebay with %s\n", args[0])
  moviedb = storage.NewMoviedb(config.Moviedbpath, config.Convertpath)
  moviedb.Restore()

  // start converting broker
  broker := converter.NewBroker(moviedb)
  go broker.Run()

  quit := make(chan bool)
  go net.StartWebServer(config, moviedb, quit)

  mediaFiles := storage.Scan(config.Moviefspath)

  tmdb := utils.NewTmdb()
  for _, file := range mediaFiles {
    if m := moviedb.FindByName(file.Name); m == nil {
      tmdbMovie := tmdb.Lookup(file.Name, file.Year)
      if tmdbMovie != nil {
        movie := storage.NewMovieFromTmdb(tmdbMovie, file)
        err := moviedb.SaveMovie(movie)
        if err != nil {
          panic(err)
        }
        moviedb.AddMovie(movie)
      } else {
        log.Printf("SKIP: %s\n", file.Name)
      }
    }
  }

  log.Printf("Moviedb: %d", moviedb.Size())

  <-quit
}
