FROM golang

VOLUME /moviefs
VOLUME /moviedb
VOLUME /convert
VOLUME /config

WORKDIR /go/src/gitlab.com/themoviebay/moviebay

ENV SRC=/usr/local

RUN echo "deb http://www.deb-multimedia.org jessie main non-free" >> /etc/apt/sources.list \
  && echo "deb-src http://www.deb-multimedia.org jessie main non-free" >> /etc/apt/sources.list \
  && apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 5C808C2B65558117

RUN buildDeps="autoconf \
  automake \
  cmake \
  curl \
  bzip2 \
  g++ \
  gcc \
  git \
  libtool \
  make \
  nasm \
  perl \
  pkg-config \
  python \
  libssl-dev \
  yasm \
  zlib1g-dev" \
&& export MAKEFLAGS="-j$(($(nproc) + 1))" \
&& apt-get update && apt-get install -y --no-install-recommends \
  ${buildDeps} \
  build-essential \
  libmp3lame-dev \
  libvorbis-dev \
  libtheora-dev \
  libspeex-dev \
  libavdevice-dev \
  yasm \
  pkg-config \
  libopenjpeg-dev \
  libx264-dev

RUN wget http://ffmpeg.org/releases/ffmpeg-3.3.1.tar.bz2 \
  && tar xvjf ffmpeg-3.3.1.tar.bz2 \
  && cd ffmpeg-3.3.1 \
  && ./configure \
    --prefix="${SRC}" \
    --bindir="${SRC}/bin" \
    --disable-static \
    --enable-gpl \
    --enable-postproc \
    --enable-swscale \
    --enable-avfilter \
    --enable-libmp3lame \
    --enable-libvorbis \
    --enable-libtheora \
    --enable-libx264 \
    --enable-libspeex \
    --enable-shared \
    --enable-pthreads \
    --enable-libopenjpeg \
    --enable-nonfree \
    --enable-shared \
  && make \
  && make install \
  && make distclean

RUN apt-get purge -y ${buildDeps} \
  && apt-get autoremove -y \
  && apt-get clean -y \
  && rm -rf /var/lib/apt/lists \
  && rm -rf ffmpeg-3.3.1 \
  && ldconfig \
  && ffmpeg -buildconf

ADD . /go/src/gitlab.com/themoviebay/moviebay
RUN cd /go/src/gitlab.com/themoviebay/moviebay \
  && go get -d \
  && go build

ENTRYPOINT /go/src/gitlab.com/themoviebay/moviebay/moviebay /config/config.json

EXPOSE 9000
