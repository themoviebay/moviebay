package config

import(
  "encoding/json"
  "os"
)

type Config struct {
  TmdbApiKey string
  Moviefspath string
  Moviedbpath string
  Convertpath string
}

var config *Config
var configLoaded = false

func NewConfig(file string) *Config {
  if !configLoaded {
    file, _ := os.Open(file)
    config = &Config{}
    decoder := json.NewDecoder(file)
    err := decoder.Decode(config)
    if err != nil {
      panic(err)
    }
    configLoaded = true
  }

  return config
}

func Load() *Config {
  return config
}
