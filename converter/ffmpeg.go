package converter

import(
  "log"
  "time"
  "os/exec"
)

type Ffmpeg struct {
  running bool
  start time.Time
  duration time.Duration
}

func (f *Ffmpeg) Start(in string, out string, done chan bool) {
  f.running = true
  log.Println("Start ffmpeg process")
  cmd := exec.Command("ffmpeg", "-i", in, "-c:v", "libx264", "-preset", "fast", "-threads", "1", out)
  f.start = time.Now()
  // cmd.Stdout = os.Stdout
  // cmd.Stderr = os.Stderr
  cmd.Start()
  go func() {
    if err := cmd.Wait(); err != nil {
      panic(err)
    }
    f.duration = time.Since(f.start)
    f.running = false
    done <- true
  }()
}

func (f *Ffmpeg) RuntimeMin() time.Duration {
  return f.duration / time.Millisecond / time.Second / time.Minute
}

func (f *Ffmpeg) IsRunning() bool {
  return f.running
}

func NewFfmpeg() *Ffmpeg {
  return &Ffmpeg{}
}
