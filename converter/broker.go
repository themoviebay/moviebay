package converter

import(
  "log"
  "gitlab.com/themoviebay/moviebay/storage"
  "gitlab.com/themoviebay/moviebay/config"
)

type Broker struct {
  db *storage.Moviedb
  jobs chan *storage.Movie
  done chan *storage.Movie
  queue []*storage.Movie
  current *storage.Movie
  running bool
}

type Status struct {
  Current int `json:"current"`
  Queue []int `json:"queue"`
}

var broker *Broker

func (b *Broker) ConvertMovie(movie *storage.Movie) {
  b.jobs <- movie
}

func (b *Broker) Run() {
  ffmpeg := NewFfmpeg()
  conf := config.Load()
  for {
    select {
    case movie := <-b.jobs:
      if b.running {
        b.queue = append(b.queue, movie)
        log.Printf("Added %s to converting queue\n", movie)
      } else {
        go func() {
          done := make(chan bool)
          log.Printf("Start converting %s\n", movie)
          ffmpeg.Start(movie.OriginalPath, conf.Convertpath + "/" + movie.IdStr() + ".mp4", done)
          <-done
          b.done <- movie
        }()
        b.current = movie
        b.running = true
      }
    case movie := <-b.done:
      movie.Converted = true
      movie.ConvertedPath = conf.Convertpath + "/" + movie.IdStr() + ".mp4"
      b.db.SaveMovie(movie)
      log.Printf("Converting finished %s\n", movie)
      b.running = false
      b.current = nil
      go b.next()
    }
  }
}

func (b *Broker) next() {
  if len(b.queue) > 0 {
    b.jobs <- b.queue[0]
    if (len(b.queue) - 1) == 0 {
      b.queue = b.queue[:0]
    } else {
      b.queue = b.queue[1:len(b.queue)]
    }
  } else {
    log.Println("Queue empty")
  }
}

func (b *Broker) Status() Status {
  if b.running {
    queue := make([]int, 0)
    for _, movie := range b.queue {
      queue = append(queue, movie.Id)
    }
    return Status{b.current.Id, queue}
  } else {
    return Status{0, make([]int, 0)}
  }
}

func NewBroker(moviedb *storage.Moviedb) *Broker {
  broker = &Broker{
    moviedb,
    make(chan *storage.Movie),
    make(chan *storage.Movie),
    make([]*storage.Movie, 0),
    nil,
    false,
  }
  return LoadBroker()
}

func LoadBroker() *Broker {
  return broker
}
