# Moviebay

Moviebay is a very small project the manage you own movie library. The goal of this project
is to watch your own Movies over the internet in a private environment.

This project is currently in alpha state and not well tested, at the moment I'm experimenting
with some solution in some different ways.

* HTML5 Video
* Converting/Streaming/FFMPEG

This project is also a nice playground for me to train my go skills.

## Features / Whats included

* Run Moviebay on Linux / Windows / MacOS
* Run Moviebay in Docker
* Moviebay ships with a frontend included
* Match movies with [TMDB-API][1]
* Show your movies in the frontend
* It's possible to stream a HTML5 video

## Installation

### Go Workspace

The easiest way is to run Moviebay inside your Go Workspace. Follow the install guide from [Go][2].

Navigate to `src` folder with `cd $GOPATH/src`.
```bash
go get gitlab.com/themoviedb/moviedb
cd gitlab.com/themoviedb/moviedb
go build
```

Now start Moviebay `./moviebay config.json`.
> NOTE: You've to modify your config.dist.json.

### Docker

The Docker way is another way to run Moviedb
```bash
docker pull registry.gilab.com/themoviebay/moviebay/moviebay
docker run registry.gilab.com/themoviebay/moviebay/moviebay \
  -v /path/to/movies:/moviefspath \
  -v /path/to/moviedb:/moviedbpath \
  -v /path/to/converted/movies:/converted \
  -v /path/to/config.json:/config/config.json
```

If the Docker container up and running go to http://localhost:9000

## Configuration

The configuration is easy...

```json
{
  "TmdbApiKey": "",
  "Moviefspath": "/moviefs",
  "Moviedbpath": "/moviedb",
  "Convertpath": "/convert"
}
```

* TmdbApiKey - Is needed for movie database lookup and covers
* Moviefspath - Where you store your original movie files
* Moviedbpath - Used to save the results from TMDB
* Convertpath - Outputpath for converted files


[1]: https://www.themoviedb.org/documentation/api
[2]: https://golang.org/doc/install
