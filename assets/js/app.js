/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const dashboard = Vue.component('dashboard', require('./components/Dashboard.vue'));
const player = Vue.component('player', require('./components/Player.vue'));

const router = new VueRouter({
  routes: [
    {path: '/', component: dashboard, name: 'index'},
    {path: '/play/:mId', component: player, name: 'player'}
  ]
});

const app = new Vue({
    el: '#app',
    router: router,
});
