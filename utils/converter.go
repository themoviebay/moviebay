package utils

import(
  // "log"
  "os/exec"
  // "os"
)

func check(err error) {
  if err != nil {
    panic(err)
  }
}

type Converter struct {
  Id int
  Process *exec.Cmd
  Done chan error
  IsDone bool
}

func NewConverter() *Converter {
  return &Converter{}
}

func (c *Converter) Kill() error {
  return c.Process.Process.Kill()
}

func (c *Converter) Start(id int, in string, out string) {
  c.Process = exec.Command("ffmpeg", "-i", in, "-c:v", "libx264", "-preset", "medium", "-threads", "2", out)
  c.Id = id
  c.Process.Start()
  c.IsDone = false
  go func() {
    err := c.Process.Wait()
    check(err)
    c.IsDone = true
    c.Id = 0
  }()
}

// func StartConverter(in, out string) {
//   // args := "-i " + "'/Users/lindsey/Documents/moviestorage/The Accountant (2016)/The Accountant (2016).mkv'" + " -c:v libx264 -preset medium " + out
//   // log.Println(args)
//   cmd := exec.Command("ffmpeg", "-i", "/Users/lindsey/Documents/moviestorage/The Accountant (2016)/The Accountant (2016).mkv", "-c:v", "libx264", "-preset", "medium", "-threads", "1", out)
//   cmd.Stdout = os.Stdout
//   cmd.Stderr = os.Stderr
//   err := cmd.Run()
//   check(err)
// }
