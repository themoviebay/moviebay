package utils

import(
  "log"
  "net/http"
  "net/url"
  "io/ioutil"
  "strings"
  "encoding/json"
)

const tmdbUrl =  "https://api.themoviedb.org/3/search/movie"

type Tmdb struct {

}

type TmdbMovie struct {
  Id int
  Title string
  Original_title string
  Overview string
  Release_date string
  Poster_path string
  Backdrop_path string
}

type TmdbSearchResults struct {
  Page int
  Results []*TmdbMovie
  Total_results int
}



func NewTmdb() Tmdb {
  return Tmdb{}
}

func (t Tmdb) Lookup(name string, year string) *TmdbMovie {
  log.Printf("Lookup Query: %s %s\n", name, year)
  qUrl, _ := url.Parse(tmdbUrl)
  params := url.Values{}
  params.Add("query", name)
  params.Add("year", year)
  params.Add("include_adult", "true")
  params.Add("language", "de")
  params.Add("api_key", "280b6c5e0a196cab61823305d553390a")
  qUrl.RawQuery = params.Encode()
  log.Println(qUrl.String())

  payload := strings.NewReader("{}")
  req, _ := http.NewRequest("GET", qUrl.String(), payload)
  res, _ := http.DefaultClient.Do(req)
  body, _ := ioutil.ReadAll(res.Body)
  defer res.Body.Close()

  var result TmdbSearchResults
  json.Unmarshal(body, &result)
  if result.Total_results > 0 {
    return result.Results[0]
  }
  return nil
}
