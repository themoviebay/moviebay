const { mix } = require('laravel-mix');
let ImageminPlugin = require('imagemin-webpack-plugin').default;

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.webpackConfig( {
  plugins: [
    new ImageminPlugin( {
      pngquant: {
        quality: '95-100',
      },
      test: /\.(jpe?g|png|gif|svg)$/i,
    } ),
  ],
});

mix.setPublicPath('public_html')
  .js('assets/js/app.js', 'public_html/js')
  .sass('assets/sass/app.scss', 'public_html/css')
  .copy('assets/images', 'public_html/images', false);
