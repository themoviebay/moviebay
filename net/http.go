package net

import(
  "net/http"
  "encoding/json"
  "log"
  "strconv"
  "github.com/gorilla/mux"
  "gitlab.com/themoviebay/moviebay/storage"
  "gitlab.com/themoviebay/moviebay/config"
  "gitlab.com/themoviebay/moviebay/converter"
)

var moviedb *storage.Moviedb
var conf *config.Config

func check(err error) {
  if err != nil {
    panic(err)
  }
}

type ResponseState struct {
  status string
}

func index(w http.ResponseWriter, r *http.Request) {
  if r.URL.Path == "/" {
    log.Println("GET /")
    http.ServeFile(w, r, "public_html/index.html")
  } else {
    log.Println(r.URL.Path)
    http.ServeFile(w, r, "public_html" + r.URL.Path)
  }
}

func movies(w http.ResponseWriter, r *http.Request) {
  log.Println("GET /movies")
  movies := moviedb.All()
  type JsonResponse struct {
    Movies []*storage.Movie `json:"movies"`
  }
  jResp := &JsonResponse{movies}
  response, _ := json.Marshal(jResp)
  w.Header().Set("Content-Type", "application/json")
  w.Write(response)
}

func movie(w http.ResponseWriter, r *http.Request) {
  vars := mux.Vars(r)
  log.Println("GET /movies/" + vars["id"])
  id, _ := strconv.Atoi(vars["id"])
  movie := moviedb.FindById(id)
  type JsonResponse struct {
    Movie *storage.Movie `json:"movie"`
  }
  jResp := &JsonResponse{movie}
  response, _ := json.Marshal(jResp)
  w.Header().Set("Content-Type", "application/json")
  w.Write(response)
}

func kill(w http.ResponseWriter, r *http.Request) {
  // converter.Kill()
  resp := ResponseState{"ok"}
  respJson, _ := json.Marshal(resp)
  w.Header().Set("Content-Type", "application/json")
  w.Write(respJson)
}

func convertHandler(w http.ResponseWriter, r *http.Request) {
  broker := converter.LoadBroker()
  vars := mux.Vars(r)
  id, _ := strconv.Atoi(vars["id"])
  movie := moviedb.FindById(id)
  broker.ConvertMovie(movie)
  // converter.Start(movie.TmdbId, movie.Meta.Path, conf.Convertpath + "/" + vars["id"] + ".mp4")
  w.Header().Set("Content-Type", "application/json")
  type IdResp struct {
    Id int
  }
  resp := &IdResp{movie.Id}
  respJson, _ := json.Marshal(resp)
  w.Write(respJson)
}

func showConvert(w http.ResponseWriter, r *http.Request) {
  broker := converter.LoadBroker()
  type JsonResponse struct {
    Status converter.Status `json:"status"`
  }
  jResp := &JsonResponse{broker.Status()}
  response, _ := json.Marshal(jResp)
  w.Header().Set("Content-Type", "application/json")
  w.Write(response)
}

func StartWebServer(config *config.Config, db *storage.Moviedb, quit chan bool) {
  moviedb = db
  conf = config
  r := mux.NewRouter()

  r.HandleFunc("/movies/{id:[0-9]+}", movie)
  r.HandleFunc("/movies", movies)
  r.HandleFunc("/play/{id:[0-9]+}", StreamMovieHandler)
  r.HandleFunc("/convert/{id:[0-9]+}", convertHandler)
  r.HandleFunc("/convert", showConvert)
  r.HandleFunc("/kill", kill)
  r.PathPrefix("/css/").Handler(http.FileServer(http.Dir("public_html")))
  r.PathPrefix("/js/").Handler(http.FileServer(http.Dir("public_html")))
  r.PathPrefix("/fonts/").Handler(http.FileServer(http.Dir("public_html")))
  r.PathPrefix("/images/").Handler(http.FileServer(http.Dir("public_html")))
  r.HandleFunc("/", index)

  err := http.ListenAndServe(":9000", r)
  check(err)
}
