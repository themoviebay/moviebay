package net

import(
  "net/http"
  "regexp"
  "os"
  "io"
  "fmt"
  "log"
  "strconv"
  "bufio"
  "github.com/gorilla/mux"
  "gitlab.com/themoviebay/moviebay/config"
)

// Stream the movie file with Status 206
func StreamMovieHandler(w http.ResponseWriter, r *http.Request) {
  defer func() {
    if r := recover(); r != nil {
      log.Println("Recovered in f", r)
    }
  }()
  vars := mux.Vars(r)
  config := config.Load()
  filePath := config.Convertpath + "/" + vars["id"] + ".mp4"
  rangeHeader := r.Header.Get("Range")
  seekStart, seekEnd := parseRangeHeader(rangeHeader)

  // Open file and get info
  file, err := os.Open(filePath)
  check(err)
  fileInfo, err := file.Stat()
  check(err)

  defer func() {
    err := file.Close()
    check(err)
  }()

  // Set default response header
  w.Header().Set("Connection", r.Header.Get("Connection"))
  w.Header().Set("Content-Type", "video/mp4")

  // Send diffrent headers for range request or non range request
  if seekEnd == 0 {
    seekEnd = fileInfo.Size() - 1
  }

  // Send diffrent headers for range request or non range request
  if rangeHeader == "" {
    w.Header().Set("Content-Length", toString(fileInfo.Size()))
    w.Header().Set("Accept-Ranges", "bytes")
  } else {
    w.Header().Set("Content-Length", toString(seekEnd - seekStart + 1)) // Nothing works with this... empty request
    w.Header().Set("Content-Range", formatToRangeHeader(seekStart, seekEnd, fileInfo.Size()))
    w.WriteHeader(http.StatusPartialContent)
  }

  // Stream content
  var transferred int64 = 0
  buf := make([]byte, 1024 * 4) // ReadBuffer - 4Kb
  chunk := seekEnd - seekStart + 1
  file.Seek(seekStart, 0)
  reader := bufio.NewReader(file)
  writer := bufio.NewWriter(w)

  fmt.Printf("start: %d end: %d chunk: %d lenth: %d\n", seekStart, seekEnd, chunk, fileInfo.Size())

  for {
    n, err := reader.Read(buf)
    if n == 0 {
      break
    }
    if err != nil && err != io.EOF {
      panic(err)
    }
    transferred += int64(n)

    // check if the correct size was transferred
    diff := transferred - chunk
    if diff > 0 { // cut buffer, to large; chunk size reached
      end := int64(n) - diff
      _, err = writer.Write(buf[:end])
      check(err)
    } else {
      _, err = writer.Write(buf[:n])
      check(err)
    }

    err = writer.Flush()
    check(err)
  }
}

// Parse RANGE HTTP header and returns
// the requested start and end bytes
// start end end bytes are automatically calculated
// if the header was not complete
func parseRangeHeader(header string) (int64, int64) {
  var seekStart, seekEnd int64 = 0, 0

  // specify regex for diffrent header versions
  regStartOnly := regexp.MustCompile("bytes=(\\d+)-") // only start value is present
  regEndOnly := regexp.MustCompile("bytes=-(\\d+)") // only end value is present
  regFull := regexp.MustCompile("bytes=(\\d+)-(\\d+)") // both start and end values are present

  // check vor header version
  if regStartOnly.MatchString(header) {
    parts := regStartOnly.FindStringSubmatch(header)
    seekStart = toInt64(parts[1])
  } else if (regEndOnly.MatchString(header)) {
    parts := regEndOnly.FindStringSubmatch(header)
    seekEnd = toInt64(parts[1])
  } else if (regFull.MatchString(header)) {
    parts := regFull.FindStringSubmatch(header)
    seekStart, seekEnd = toInt64(parts[1]), toInt64(parts[2])
  }

  return seekStart, seekEnd
}

// Format a correct http RANGE header for the response
func formatToRangeHeader(start, end, size int64) string {
  // @TODO Maybe there is a better option, fmt.Sprintf looks
  // not so good
  return fmt.Sprintf("bytes %d-%d/%d", start, end, size)
}

// Helper function to convert string to int64
func toInt64(s string) int64 {
  val, _ := strconv.ParseInt(s, 10, 64)
  return val
}

// Helper function to convert int64 to string
func toString(v int64) string {
  val := strconv.FormatInt(v, 10)
  return val
}
